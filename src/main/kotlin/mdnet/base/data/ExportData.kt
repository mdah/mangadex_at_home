package mdnet.base.dao

import com.fasterxml.jackson.annotation.JsonProperty
import mdnet.base.export.util.ExportCommonsEnum

data class ExportData(
    var identifier: String = ExportCommonsEnum.NONE.toString(),
    @get:JsonProperty("isCached") var isCached: Boolean = false,
    var executionTimeInMs: Long = 0,
    @get:JsonProperty("isDataSaver") var isDataSaver: Boolean = false,
    var bytesSent: Long = 0,
    @get:JsonProperty("isBrowserCached") var isBrowserCached: Boolean = false,
    var requestIP: String = ExportCommonsEnum.NONE.toString(),
    var date: Long = 0L,
    var systemByteSent: Long = 0,
    var systemBytesHDD: Long = 0,
    var referer: String = ExportCommonsEnum.NONE.toString(),
    var refererblocked: Boolean = false,
    var tokenError: String = ExportCommonsEnum.NONE.toString()
) {
    fun reset() {
        identifier = ExportCommonsEnum.NONE.toString()
        isCached = false
        executionTimeInMs = 0
        isDataSaver = false
        bytesSent = 0
        isBrowserCached = false
        requestIP = ExportCommonsEnum.NONE.toString()
        date = 0L
        systemByteSent = 0
        systemBytesHDD = 0
        referer = ExportCommonsEnum.NONE.toString()
        refererblocked = false
        tokenError = ExportCommonsEnum.NONE.toString()
    }
}
