package mdnet.base.export.executor

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.FutureTask
import java.util.concurrent.RunnableFuture
import java.util.concurrent.ThreadPoolExecutor
import mdnet.base.export.exporter.HttpGetRunner
import mdnet.base.export.util.ResponseData
import mdnet.base.settings.ClientSettings
import org.slf4j.LoggerFactory

abstract class AbstractHttpGetExecutor() {

    val LOGGER = LoggerFactory.getLogger("Get-Executor")

    object ThreadPool {
        // don't use more than 4 threads for http-requests
        private val executor: ExecutorService = Executors.newFixedThreadPool(100)
        private val tpe = executor as ThreadPoolExecutor

        fun get(): ExecutorService? {
            return executor
        }

        fun getQueueSize(): Int {
            return tpe.queue.size
        }
    }

    abstract fun get(url: String, clientSettings: ClientSettings): ResponseData

    fun execute(exporter: HttpGetRunner): ResponseData {
        if (LOGGER.isTraceEnabled) {
            LOGGER.trace("Nbr Requests in Queue: " + ThreadPool.getQueueSize())
        }
        ThreadPool.get()!!.execute(exporter)
        return exporter.responseData
    }

    fun executeAndWait(exporter: HttpGetRunner): ResponseData {
        val task: RunnableFuture<Void?> = FutureTask(exporter, null)
        ThreadPool.get()!!.execute(task)
        task.get()
        return exporter.responseData
    }

    fun shutdown() {
        ThreadPool.get()!!.shutdown()
        if (LOGGER.isTraceEnabled) {
            LOGGER.trace("Shutting down thread pool for export")
        }
        while (!ThreadPool.get()!!.isTerminated) {}
    }
}
