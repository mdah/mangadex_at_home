package mdnet.base.export.executor

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.ThreadPoolExecutor
import mdnet.base.dao.ExportData
import mdnet.base.export.exporter.HttpPostRunner
import mdnet.base.settings.ClientSettings
import org.slf4j.LoggerFactory

abstract class AbstractHttpPostExecutor() {

    val LOGGER = LoggerFactory.getLogger("Post-Executor")

    object ThreadPool {
        // don't use more than 4 threads for http-requests
        private val executor: ExecutorService = Executors.newFixedThreadPool(4)
        private val tpe = executor as ThreadPoolExecutor

        fun get(): ExecutorService? {
            return executor
        }

        fun getQueueSize(): Int {
            return tpe.queue.size
        }
    }

    abstract fun send(exportData: ExportData, clientSettings: ClientSettings)

    fun execute(exporter: HttpPostRunner) {
        if (LOGGER.isTraceEnabled) {
            LOGGER.trace("Nbr Requests in Queue: " + ThreadPool.getQueueSize())
        }
        ThreadPool.get()!!.execute(exporter)
    }

    fun shutdown() {
        ThreadPool.get()!!.shutdown()
        if (LOGGER.isTraceEnabled) {
            LOGGER.trace("Shutting down thread pool for export")
        }
        while (!ThreadPool.get()!!.isTerminated) {}
    }
}
