package mdnet.base.export.executor

import mdnet.base.export.exporter.ImageHttpGetRunner
import mdnet.base.export.util.ResponseData
import mdnet.base.settings.ClientSettings

class HttpGetExecutor() : AbstractHttpGetExecutor() {

    override fun get(url: String, clientSettings: ClientSettings): ResponseData {
        var imagehttpGetRunner: ImageHttpGetRunner = ImageHttpGetRunner(url, clientSettings)
        executeAndWait(imagehttpGetRunner)
        return imagehttpGetRunner.responseData
    }
}
