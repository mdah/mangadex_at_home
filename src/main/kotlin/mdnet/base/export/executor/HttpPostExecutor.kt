package mdnet.base.export.executor

import mdnet.base.dao.ExportData
import mdnet.base.export.exporter.ElasticsearchHttpPostRunner
import mdnet.base.export.util.ExportTypes
import mdnet.base.settings.ClientSettings

class HttpPostExecutor() : AbstractHttpPostExecutor() {

    override fun send(exportData: ExportData, clientSettings: ClientSettings) {
        // do we send to elasticsearch?
        if (ExportTypes.valueOf(clientSettings.exportSettings!!.exportType) == ExportTypes.elasticsearch) {
            execute(ElasticsearchHttpPostRunner(exportData, clientSettings))
        }
    }
}
