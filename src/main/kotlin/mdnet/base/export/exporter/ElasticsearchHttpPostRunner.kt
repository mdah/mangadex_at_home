package mdnet.base.export.exporter

import com.fasterxml.jackson.databind.ObjectMapper
import java.util.Base64
import mdnet.base.dao.ExportData
import mdnet.base.settings.ClientSettings

class ElasticsearchHttpPostRunner(private val exportData: ExportData, private val clientSettings: ClientSettings) : HttpPostRunner() {

    override fun run() {

        // handle authToken
        var authToken = if (!clientSettings.exportSettings?.exportBase64Token.isNullOrEmpty()) {
            clientSettings.exportSettings?.exportBase64Token
        } else null

        authToken = if (authToken == null && !clientSettings.exportSettings?.exportUser.isNullOrBlank() &&
                !clientSettings.exportSettings?.exportPassword.isNullOrEmpty()) {
            val token: String = clientSettings.exportSettings!!.exportUser + ":" + clientSettings.exportSettings.exportPassword
            Base64.getEncoder().encodeToString(token.toByteArray())
        } else
            null

        val url = clientSettings.exportSettings!!.exportUrl + ":" + clientSettings.exportSettings.exportPort + "/" + clientSettings.exportSettings.exportIndex + "/_doc/"
        val body = ObjectMapper().writeValueAsString(exportData)
        sendJSONPost(url, body, authToken)

        /*
        //create request
        val request = Request(Method.POST, clientSettings.exportSettings!!.exportUrl + ":" + clientSettings.exportSettings.exportPort + "/" + clientSettings.exportSettings.exportIndex + "/_doc/")
                .header("Content-Type", "application/json" )
                .let {
                    if (authToken != null) {
                        it.header("Authorization", "Basic " + authToken)
                    } else {
                        it
                    }
                }
                .body( ObjectMapper().writeValueAsString(exportData))


        //setup synchronous client
        val client = ApacheClient()
        //send request
        val response = client(request)

        if (LOGGER.isInfoEnabled)
            LOGGER.info("Statistics send: " + response.status)
        if (LOGGER.isTraceEnabled)
            LOGGER.info("body: " + response.body)

        response.close()

 */
    }
}
