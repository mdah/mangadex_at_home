package mdnet.base.export.exporter

import java.net.HttpURLConnection
import java.net.URL
import java.security.SecureRandom
import java.security.cert.X509Certificate
import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager
import mdnet.base.export.util.ResponseData
import mdnet.base.settings.ClientSettings
import org.slf4j.LoggerFactory

abstract class HttpGetRunner : Runnable {
    val LOGGER = LoggerFactory.getLogger("Get-Runner")
    var responseData: ResponseData = ResponseData()

    fun createSocketFactory(protocols: List<String>) =
            SSLContext.getInstance(protocols[0]).apply {
                val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                    override fun getAcceptedIssuers(): Array<X509Certificate> = arrayOf()
                    override fun checkClientTrusted(certs: Array<X509Certificate>, authType: String) = Unit
                    override fun checkServerTrusted(certs: Array<X509Certificate>, authType: String) = Unit
                })
                init(null, trustAllCerts, SecureRandom())
            }.socketFactory

    fun get(url: String, clientSettings: ClientSettings) {

        URL(url)
                .openConnection()
                .let { it as HttpsURLConnection }
                .apply {
                    requestMethod = "GET"
                    connectTimeout = 10 * 1000 // 10 seconds timeout
                    let {
                        if (clientSettings.devSettings?.ignoreSSL == true) {
                            sslSocketFactory = createSocketFactory(listOf("TLSv1.2"))
                        }
                    }
                }.let {
                    if (LOGGER.isInfoEnabled)
                        LOGGER.info("requested from upstream: " + it.responseCode)
                    responseData.headers = it.headerFields
                    responseData.responseCode = it.responseCode
                    if (it.responseCode == HttpURLConnection.HTTP_OK) it.inputStream else it.errorStream
                }.let {
                    responseData.inputStream = it
                }
    }
}
