package mdnet.base.export.exporter

import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import org.slf4j.LoggerFactory

abstract class HttpPostRunner : Runnable {
    val LOGGER = LoggerFactory.getLogger("Exporter")

    fun sendJSONPost(url: String, jsonBody: String, authToken: String?) {

        URL(url)
                .openConnection()
                .let { it as HttpURLConnection }
                .apply {
                    setRequestProperty("Content-Type", "application/json; charset=utf-8")
                        if (authToken != null) {
                            setRequestProperty("Authorization", "Basic $authToken")
                        }
                    requestMethod = "POST"
                    doOutput = true
                    connectTimeout = 5 * 1000 // 5 seconds timeout
                    val outputWriter = OutputStreamWriter(outputStream)
                    outputWriter.write(jsonBody)
                    outputWriter.flush()
                }.let {
                    if (LOGGER.isInfoEnabled)
                        LOGGER.info("Statistics send: " + it.responseCode)
                    if (it.responseCode == HttpURLConnection.HTTP_OK || it.responseCode == HttpURLConnection.HTTP_CREATED) it.inputStream else it.errorStream
                }.let { streamToRead ->
                    BufferedReader(InputStreamReader(streamToRead)).use {
                        val response = StringBuffer()
                        var inputLine = it.readLine()
                        while (inputLine != null) {
                            response.append(inputLine)
                            inputLine = it.readLine()
                        }
                        it.close()

                        if (LOGGER.isTraceEnabled)
                            LOGGER.info("response body: $response")
                    }
                }
    }
}
