package mdnet.base.export.exporter

import mdnet.base.settings.ClientSettings

class ImageHttpGetRunner(private val url: String, private val clientSettings: ClientSettings) : HttpGetRunner() {

    override fun run() {
//        val body = ObjectMapper().writeValueAsString(exportData)
        get(url, clientSettings)
    }
}
