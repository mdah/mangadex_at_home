package mdnet.base.export.util

enum class ExportTypes {
    elasticsearch
}

enum class TokenErrors {
    INVALID,
    EXPIRED,
    INAPPLICABLE
}

enum class ExportCommonsEnum {
    NONE
}
