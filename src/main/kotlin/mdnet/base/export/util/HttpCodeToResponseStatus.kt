package mdnet.base.export.util

import org.http4k.core.Status

class HttpCodeToResponseStatus() {

    val httpHashMap: HashMap<Int, Status> = hashMapOf(
            100 to Status.CONTINUE,
            101 to Status.SWITCHING_PROTOCOLS,

            200 to Status.OK,
            201 to Status.CREATED,
            202 to Status.ACCEPTED,
            203 to Status.NON_AUTHORITATIVE_INFORMATION,
            204 to Status.NO_CONTENT,
            205 to Status.RESET_CONTENT,
            206 to Status.PARTIAL_CONTENT,

            300 to Status.MULTIPLE_CHOICES,
            301 to Status.MOVED_PERMANENTLY,
            302 to Status.FOUND,
            303 to Status.SEE_OTHER,
            304 to Status.NOT_MODIFIED,
            305 to Status.USE_PROXY,
            307 to Status.TEMPORARY_REDIRECT,
            308 to Status.PERMANENT_REDIRECT,

            400 to Status.BAD_REQUEST,
            401 to Status.UNAUTHORIZED,
            402 to Status.PAYMENT_REQUIRED,
            403 to Status.FORBIDDEN,
            404 to Status.NOT_FOUND,
            405 to Status.METHOD_NOT_ALLOWED,
            406 to Status.NOT_ACCEPTABLE,
            407 to Status.PROXY_AUTHENTICATION_REQUIRED,
            408 to Status.REQUEST_TIMEOUT,
            409 to Status.CONFLICT,
            410 to Status.GONE,
            411 to Status.LENGTH_REQUIRED,
            412 to Status.PRECONDITION_FAILED,
            413 to Status.REQUEST_ENTITY_TOO_LARGE,
            414 to Status.REQUEST_URI_TOO_LONG,
            415 to Status.UNSUPPORTED_MEDIA_TYPE,
            416 to Status.REQUESTED_RANGE_NOT_SATISFIABLE,
            417 to Status.EXPECTATION_FAILED,
            418 to Status.I_M_A_TEAPOT,
            422 to Status.UNPROCESSABLE_ENTITY,
            426 to Status.UPGRADE_REQUIRED,
            429 to Status.TOO_MANY_REQUESTS,

            500 to Status.INTERNAL_SERVER_ERROR,
            501 to Status.NOT_IMPLEMENTED,
            502 to Status.BAD_GATEWAY,
            503 to Status.SERVICE_UNAVAILABLE,
            503 to Status.CONNECTION_REFUSED,
            503 to Status.UNKNOWN_HOST,
            504 to Status.GATEWAY_TIMEOUT,
            504 to Status.CLIENT_TIMEOUT,
            505 to Status.HTTP_VERSION_NOT_SUPPORTED
    )

    fun getResponseStatus(httpCode: Int): Status {
        return httpHashMap.get(httpCode)!!
    }
}
