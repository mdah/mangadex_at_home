package mdnet.base.export.util

import java.io.InputStream

class ResponseData() {

    var headers: Map<String, List<String>>? = null
    var responseCode: Int = 0
    var inputStream: InputStream? = null
}
