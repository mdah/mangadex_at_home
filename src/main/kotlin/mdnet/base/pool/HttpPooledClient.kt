package mdnet.base.pool

import java.net.InetAddress
import java.util.concurrent.TimeUnit
import mdnet.base.settings.ClientSettings
import org.apache.http.client.config.CookieSpecs
import org.apache.http.client.config.RequestConfig
import org.apache.http.impl.client.HttpClients
import org.http4k.client.ApacheClient
import org.http4k.core.BodyMode
import org.http4k.core.HttpHandler

private const val THREADS_TO_ALLOCATE = 262144 // 2**18

class HttpPooledClient() {
    private fun getAddress(settings: ClientSettings): InetAddress {
        return if (settings.clientHostname != "0.0.0.0") {
            InetAddress.getByName(settings.clientHostname)
        } else {
            InetAddress.getLocalHost()
        }
    }

    fun getClient(settings: ClientSettings): HttpHandler {

        return ApacheClient(responseBodyMode = BodyMode.Stream, client = HttpClients.custom()
            .disableConnectionState()
            .setDefaultRequestConfig(
                RequestConfig.custom()
                    .setCookieSpec(CookieSpecs.IGNORE_COOKIES)
                    .setConnectTimeout(3000)
                    .setSocketTimeout(3000)
                    .setConnectionRequestTimeout(3000)
                    .apply {
                        if (settings.clientHostname != "0.0.0.0") {
                            setLocalAddress(InetAddress.getByName(settings.clientHostname))
                        }
                    }
                    .build())
            .setMaxConnTotal(3000)
            .setMaxConnPerRoute(3000)
            .setConnectionTimeToLive(5, TimeUnit.MINUTES)
            .build())
    }
}
